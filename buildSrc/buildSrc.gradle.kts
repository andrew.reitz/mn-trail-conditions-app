plugins {
    `kotlin-dsl`
}

dependencies {
    implementation(kotlin("gradle-plugin", libs.versions.kotlin.get()))
    implementation(libs.android.gradle)
    implementation(libs.kotlinpoet)

    testImplementation(gradleKotlinDsl())
    testImplementation(gradleTestKit())
    implementation(kotlin("test-junit"))
    testImplementation(libs.junit)
}
