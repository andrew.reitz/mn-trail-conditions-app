package trail.gcp.aggregator.twitter

import kotlinx.coroutines.withTimeout
import cash.andrew.kotlin.common.retry
import trail.networking.twitter.TwitterAccount
import twitter4j.TweetsResponse
import twitter4j.Twitter
import twitter4j.v2

suspend fun Twitter.getTrailStatus(account: TwitterAccount): TwitterStatus {
  return retry {
    withTimeout(2000) {

      val tweets: TweetsResponse = this@getTrailStatus.v2.getUserTweets(
        userId = account.twitterId,
        maxResults = 5,
      )

      val tweet = tweets.tweets.first()

      val description = tweet.text
      val updatedTime = tweet.createdAt

      TwitterStatus(
        description = description,
        updatedTime = updatedTime?.toInstant()?.toEpochMilli() ?: System.currentTimeMillis()
      )
    }
  }
}

data class TwitterStatus(
  val description: String,
  val updatedTime: Long
) {
  val status: String get() = if (description.contains("closed", ignoreCase = true)) "Closed" else "Open"
}
