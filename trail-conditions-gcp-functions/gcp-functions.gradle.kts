plugins {
  kotlin("jvm")
  kotlin("kapt")

  alias(libs.plugins.kotlinx.serialization)
  alias(libs.plugins.shadow)

  id("twitter-api-keys")
  id("firebase-credentials")
  id("kotlin-config-writer")
}

configurations.create("invoker")

dependencies {
  implementation(project(":trail-conditions-networking"))

  implementation(kotlin("reflect"))
  implementation(libs.kotlinx.coroutines.core)

  implementation(libs.firebase.admin)
  implementation(libs.twitter4j)
  implementation(libs.twitter4j.v2)

  kapt(libs.dagger.compiler)
  implementation(libs.dagger)

  // Every function needs this dependency to get the Functions Framework API.
  compileOnly(libs.google.cloud.function.api)

  // To run function locally using Functions Framework's local invoker
  "invoker"(libs.google.cloud.function.invoker)

  testImplementation(kotlin("test-annotations-common"))
  testImplementation(kotlin("test-junit"))
  testImplementation(libs.google.cloud.function.api)
  testImplementation(libs.ktor.mock)
  testImplementation(libs.kotlinx.coroutines.test)
  testImplementation(libs.mockito.kotlin)
}

kotlinConfigWriter {
  val consumerKey: String by project
  val consumerSecret: String by project
  val accessTokenKey: String by project
  val accessTokenSecret: String by project

  val firebaseProjectId: String by project
  val firebaseClientEmail: String by project
  val firebasePrivateKey: String by project

  put("consumer_key" to consumerKey)
  put("consumer_secret" to consumerSecret)
  put("access_token_key" to accessTokenKey)
  put("access_token_secret" to accessTokenSecret)
  put("firebase_project_id" to firebaseProjectId)
  put("firebase_client_email" to firebaseClientEmail)
  put("firebase_private_key" to firebasePrivateKey)
}

apply(from = "runFunction.gradle")

tasks.named<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar>("shadowJar") {
  destinationDirectory.set(file("$buildDir/gcp"))
  mergeServiceFiles()
}

val deployAggregatorStaging = tasks.register<Exec>("deployAggregatorStaging") {
  dependsOn(tasks.named("shadowJar"))

  workingDir = project.buildDir
  commandLine = listOf(
    "gcloud",
    "functions",
    "deploy",
    "trail-aggregator-staging",
    "--entry-point",
    "trail.gcp.TrailAggregatorFunction",
    "--runtime",
    "java11",
    "--trigger-http",
    "--memory",
    "256MB",
    "--allow-unauthenticated",
    "--project",
    "mn-trail-functions",
    "--source=gcp"
  )
}

val deployNotificationStaging = tasks.register<Exec>("deployNotificationStaging") {
  dependsOn(tasks.named("shadowJar"))

  workingDir = project.buildDir
  commandLine = listOf(
    "gcloud",
    "functions",
    "deploy",
    "trail-notification-staging",
    "--entry-point",
    "trail.gcp.TrailNotificationsFunction",
    "--runtime",
    "java11",
    "--trigger-topic",
    "trailNotifications",
    "--memory",
    "256MB",
    "--project",
    "mn-trail-functions",
    "--source=gcp"
  )
}

tasks.register("deployStaging") {
  description = "publish gcp functions to staging"
  group = "deploy"
  dependsOn(deployAggregatorStaging, deployNotificationStaging)
}

val deployAggregatorRelease = tasks.register<Exec>("deployAggregatorRelease") {
  dependsOn(tasks.named("shadowJar"))

  workingDir = project.buildDir
  commandLine = listOf(
    "gcloud",
    "functions",
    "deploy",
    "trail-aggregator",
    "--entry-point",
    "trail.gcp.TrailAggregatorFunction",
    "--runtime",
    "java11",
    "--trigger-http",
    "--memory",
    "256MB",
    "--allow-unauthenticated",
    "--project",
    "mn-trail-functions",
    "--source=gcp"
  )
}

val deployNotificationRelease = tasks.register<Exec>("deployNotificationRelease") {
  dependsOn(tasks.named("shadowJar"))

  workingDir = project.buildDir
  commandLine = listOf(
    "gcloud",
    "functions",
    "deploy",
    "trail-notification",
    "--entry-point",
    "trail.gcp.TrailNotificationsFunction",
    "--runtime",
    "java11",
    "--trigger-topic",
    "trailNotifications",
    "--memory",
    "256MB",
    "--project",
    "mn-trail-functions",
    "--source=gcp"
  )
}

tasks.register("deployRelease") {
  description = "publish gcp functions to staging"
  group = "deploy"
  dependsOn(deployAggregatorRelease, deployNotificationRelease)
}

