@file:Suppress("NOTHING_TO_INLINE")

package cash.andrew.mntrailconditions.util

import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.widget.TooltipCompat
import java.util.*

inline fun View.setToolTipTextCompat(@StringRes value: Int): Unit =
        context.getString(value).replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.ROOT) else it.toString() }
                .let { TooltipCompat.setTooltipText(this, it) }
