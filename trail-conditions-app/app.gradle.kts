plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")

    alias(libs.plugins.play.publisher)
    alias(libs.plugins.google.services)
    alias(libs.plugins.firebase.crashlytics)

    alias(libs.plugins.version.check)

    id("build-number")
    id("android-signing-config")
}

play {
    serviceAccountCredentials.set(file(properties["cash.andrew.mntrail.publishKey"] ?: "keys/publish-key.json"))
    track.set("internal")
    defaultToAppBundles.set(true)
}

android {
    compileSdk = 33
    namespace = "cash.andrew.mntrailconditions"

    signingConfigs {
        getByName("debug") {
            storeFile = file("keys/debug.keystore")
            storePassword = "android"
            keyAlias = "android"
            keyPassword = "android"
        }
        create("release") {
            val keystoreLocation: String by project
            val keystorePassword: String by project
            val storeKeyAlias: String by project
            val aliasKeyPassword: String by project

            storeFile = file(keystoreLocation)
            storePassword = keystorePassword
            keyAlias = storeKeyAlias
            keyPassword = aliasKeyPassword
        }
    }

    defaultConfig {
        applicationId = "com.andrewreitz.cash.andrew.mntrailconditions"
        minSdk = 23
        targetSdk = 33

        val buildNumber: String by project
        versionCode = buildNumber.toInt()
        versionName = project.version.toString()
    }

    buildTypes {
        getByName("debug") {
            applicationIdSuffix = ".debug"
            signingConfig = signingConfigs.getByName("debug")
            isMinifyEnabled = false
            isShrinkResources = false
            extra["alwaysUpdateBuildId"] = false
            extra["enableCrashlytics"] = false
        }
        getByName("release") {
            signingConfig = signingConfigs.getByName("release")
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.txt")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    packagingOptions {
        resources {
            excludes.addAll(
                listOf(
                    "LICENSE.txt",
                    "META-INF/LICENSE.txt",
                    "META-INF/kotlinx-serialization-runtime.kotlin_module",
                    "META-INF/ktor*"
                )
            )
        }
    }

    buildFeatures {
        viewBinding = true
    }
}

dependencies {

    implementation(projects.trailConditionsNetworking)
    implementation(libs.bundles.ktor.android)

    implementation(libs.bundles.coroutines.android)

    implementation(libs.androidx.core)
    implementation(libs.androidx.constraintlayout)
    implementation(libs.androidx.annoations)
    implementation(libs.androidx.appcompat)
    implementation(libs.androidx.recyclerview)
    implementation(libs.androidx.cardview)
    implementation(libs.androidx.lifecycle.extensions)
    implementation(libs.androidx.lifecycle.viewmodel)
    implementation(libs.androidx.swiperefreshlayout)
    implementation(libs.androidx.navigation.fragment)
    implementation(libs.androidx.navigation.ui)

    implementation(libs.material)

    implementation(platform("com.google.firebase:firebase-bom:${libs.versions.firebase.get()}"))
    implementation("com.google.firebase:firebase-analytics-ktx")
    implementation("com.google.firebase:firebase-crashlytics-ktx")
    implementation("com.google.firebase:firebase-messaging")

    implementation(libs.kpermissions)

    kapt(libs.dagger.compiler)
    implementation(libs.dagger)

    implementation(libs.okhttp)
    implementation(libs.okhttp.logging)

    implementation(libs.threetenabp)
    implementation(libs.processphoenix)
    implementation(libs.timber)
    implementation(libs.byteunits)

    debugImplementation(libs.chucker)
    releaseImplementation(libs.chucker.noop)

    implementation(libs.andrew.kotlin.commons)
    implementation(libs.markwon)

    debugImplementation(libs.bundles.stetho)

    testImplementation(libs.kluent.android)
    testImplementation(libs.junit)
}

val installAll = tasks.register("installAll") {
    description = "Install all applications."
    group = "install"
    dependsOn(android.applicationVariants.map { it.installProvider })
}
