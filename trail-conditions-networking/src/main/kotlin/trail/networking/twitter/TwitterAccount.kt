package trail.networking.twitter

enum class TwitterAccount(
  val accountName: String,
  val twitterId: Long
) {

  /**
   * Cuyuna Lakes Mountain Bike Trails Conditions
   * https://twitter.com/CLMTBTCondition/
   */
  CuyunaTwitter("CLMTBTCondition", 1052793043),

  /**
   * COGGS Hartley TC
   * https://twitter.com/CoggsHartleyTC
   */
  HartleyTwitter("CoggsHartleyTC", 1849275516),

  /**
   * COGGS Lester Park
   * https://twitter.com/COGGSLesterTC
   */
  LesterParkTwitter("COGGSLesterTC", 1849454785),

  /**
   * COGGS Mission Creek
   * https://twitter.com/COGGSMissionCrk
   */
  MissionCreekTwitter("COGGSMissionCrk", 2727349693),

  /**
   * COGGS Piedmont Trail
   * https://twitter.com/COGGSPiedmontTC
   */
  PiedmontTrailTwitter("COGGSPiedmontTC", 1849372652),

  /**
   * COGGS Pokegama Trail
   * https://twitter.com/COGGSPokegamaTC
   */
  PokegamaTrailTwitter("COGGSPokegamaTC", 1849444586)
}
