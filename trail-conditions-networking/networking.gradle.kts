plugins {
  kotlin("jvm")
  kotlin("kapt")
  alias(libs.plugins.kotlinx.serialization)
}

dependencies {
  api(libs.bundles.ktor.jvm)
  api(libs.kotlinx.serialization.core)

  api(libs.andrew.kotlin.commons)

  testImplementation(kotlin("test-annotations-common"))
  testImplementation(kotlin("test-junit"))
  testImplementation(libs.ktor.mock)
  testImplementation(libs.kotlinx.coroutines.test)
  testImplementation(libs.ktor.client.mock)
}
