plugins {
    kotlin("multiplatform") apply false
    kotlin("js") apply false

    alias(libs.plugins.version.check)
}

allprojects {
    group = "cash.andrew.mntrailconditions"
    version = "10.0"

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
        kotlinOptions.jvmTarget = "1.8"
    }
}
