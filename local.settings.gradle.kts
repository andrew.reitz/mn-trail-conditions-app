includeBuild("/Users/andrewreitz/Projects/twitter4j-v2") {
    dependencySubstitution {
        substitute(module("io.github.takke:jp.takke.twitter4j-v2"))
            .with(project(":twitter4j-v2-support"))
    }
}
