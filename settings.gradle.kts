enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

pluginManagement {
  repositories {
    gradlePluginPortal()
    google()
  }
}

dependencyResolutionManagement {
  repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
  repositories {
    google()
    mavenCentral()
    maven(url = "https://jitpack.io")
  }
}

include(
  ":trail-conditions-app",
  ":trail-conditions-networking",
  ":trail-conditions-gcp-functions",
  ":trail-conditions-hosting"
)

rootProject.name = "mn-trail-conditions"

rootProject.children.forEach {
  it.buildFileName = "${it.name.replace("trail-conditions-", "")}.gradle.kts"
}

plugins {
  id("com.gradle.enterprise") version "3.2.1"
}

gradleEnterprise {
  buildScan {
    termsOfServiceUrl = "https://gradle.com/terms-of-service"
    termsOfServiceAgree = "yes"
    publishAlways()
  }
}

val localSettings = file("local.settings.gradle.kts")
if (localSettings.exists()) {
  apply(from = localSettings)
}
